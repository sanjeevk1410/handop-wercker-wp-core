# WP Core

Installs WordPress to the virtual machine and optionally installs a
WordPress Core.

## Sample Usage

    box: wercker/default
    deploy:
      steps:
        - uk-hando/wp-core:
          cwd: $WERCKER_ROOT/public/
          version: 4.8.1

&nbsp;

## Step Properties

### core-version (required)

The version of WordPress you would like to download.

* Since: `0.0.1`
* Property is `Required`
* Recommendation location: `Inline`
* `Validation` rules:
  * Must be a valid WordPress version (e.g.: `4.8.1`)

&nbsp;
