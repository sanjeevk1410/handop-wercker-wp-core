#!/bin/bash

# Property: wordpress-version1
# Check if the WordPress Version is present
if [ -z "$WERCKER_WP_CORE_CORE_VERSION" ]; then
  fail "Property wordpress-version must be defined"
fi

# Create the directory where the version will be cached
WERCKER_WP_CORE_CACHE_DIRECTORY="$WERCKER_CACHE_DIR/uk-hando/wp-core/$WERCKER_WP_CORE_CORE_VERSION/"

main() {
  if [ -d "$WERCKER_WP_CORE_CACHE_DIRECTORY" ]; then
    info "Cache directory exists, using this"
    debug "$WERCKER_WP_CORE_CACHE_DIRECTORY"
  else
    create_cache
  fi

  wordpress_install

  success "Downloaded WordPress core"
}

# Task: Update wp-cli
# Just in case the step hasn't been updated in a long time
update_wp_cli() {
  info "Checking wp-cli is at latest version"
  $WERCKER_STEP_ROOT/wp-cli.phar cli update --allow-root
  $WERCKER_STEP_ROOT/wp-cli.phar --info --allow-root
}

create_cache() {
  info "Cache directory does not exist, creating"
  debug "$WERCKER_WP_CORE_CACHE_DIRECTORY"
  mkdir -p "$WERCKER_WP_CORE_CACHE_DIRECTORY"

  update_wp_cli

  # Task: Download WordPress Core
  info "Downloading WordPress Core"
  debug "  version: $WERCKER_WP_CORE_CORE_VERSION"

  $WERCKER_STEP_ROOT/wp-cli.phar core download --force --allow-root \
  	--version="$WERCKER_WP_CORE_CORE_VERSION"] \
    --path="$WERCKER_WP_CORE_CACHE_DIRECTORY"

  WERCKER_WP_CORE_DOWNLOAD_RESPONSE=$?

  if [ "$WERCKER_WP_CORE_DOWNLOAD_RESPONSE" -ne 0 ]; then
    fail "Failed to download WordPress core"
  fi

  # Task: Remove dangerous files
  # Remove the wp-content/ folder (use the symlink)
  rm -rf "$WERCKER_WP_CORE_CACHE_DIRECTORY/wp-content/"

  # Remove the readme.html file (exposes version information)
  rm -rf "$WERCKER_WP_CORE_CACHE_DIRECTORY/readme.html"
}

wordpress_install() {
  # Task: Identify destination directory
  WERCKER_WP_CORE_PATH=`pwd`

  info "Installing WordPress"
  debug "$WERCKER_WP_CORE_PATH"

  # Task: Merge files and directories
  # Copy files from the temporary directory to the project
  cd "$WERCKER_WP_CORE_CACHE_DIRECTORY"

  find . -type d | while read WERCKER_WP_CORE_DIRNAME; do
    mkdir -p "$WERCKER_WP_CORE_PATH/$WERCKER_WP_CORE_DIRNAME"
  done

  find . -type f | while read WERCKER_WP_CORE_FILENAME; do
    cp -R "$WERCKER_WP_CORE_CACHE_DIRECTORY/$WERCKER_WP_CORE_FILENAME" \
      "$WERCKER_WP_CORE_PATH/$WERCKER_WP_CORE_FILENAME"
  done

  cd "$WERCKER_WP_CORE_PATH"
}

main
